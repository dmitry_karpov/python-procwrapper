import unittest
import time
from procwrapper.timer import RepeatableTimer


class TestRepeatableTimer(unittest.TestCase):
    def test_timer_started(self):
        """ check that timer started"""
        def test_func(*args):
            pass

        t = RepeatableTimer(10, test_func, args=('CuUXcxCY4RyAg', 'TfKs/.yU9Of4M'))
        t.start()
        self.assertTrue(t.running())
        t.cancel()

    def test_timer_stopped(self):
        """ check that timer stopped"""
        def test_func(*args):
            pass

        t = RepeatableTimer(10, test_func, args=('CuUXcxCY4RyAg', 'TfKs/.yU9Of4M'))
        t.start()
        t.cancel()
        self.assertFalse(t.running())

    def test_timer_function(self):
        """ check that timer working"""
        arg1 = 'CuUXcxCY4RyAg'
        arg2 = 'TfKs/.yU9Of4M'

        def test_func(*args):
            self.assertTupleEqual(args,(arg1, arg2))

        t = RepeatableTimer(1, test_func, args=(arg1, arg2))
        t.start()
        t._timer.join()
        # TODO: assert

    def test_timer_elapsed(self):
        """ check timer elapsed"""
        def test_func(*args):
            pass

        t = RepeatableTimer(10, test_func, args=('CuUXcxCY4RyAg', 'TfKs/.yU9Of4M'))
        t.start()
        time.sleep(2)
        self.assertAlmostEqual(t.elapsed(),2,delta=0.01)
        t.cancel()

    def test_timer_remain(self):
        """ check timer remained"""
        def test_func(*args):
            pass

        t = RepeatableTimer(10, test_func, args=('CuUXcxCY4RyAg', 'TfKs/.yU9Of4M'))
        t.start()
        time.sleep(2)
        self.assertAlmostEqual(t.remain(),10-2,delta=0.01)
        t.cancel()
