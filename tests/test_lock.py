# -*- coding: utf-8 -*-
import unittest
try:
    import mock
except ImportError:
    from unittest import mock

from procwrapper.lock import Lock
from procwrapper.timer import RepeatableTimer


class TestLock(unittest.TestCase):
    def test_instantiate(self):
        """ Lock() can be instantiated """

        lock = Lock()
        self.assertIsNotNone(lock)

    def test_default_lock_type(self):
        """Lock() default lock_type is: None """
        lock = Lock()
        self.assertIsNone(lock.lock_type)

    def test_default_lock_file(self):
        """Lock() default lock_file is: None """
        lock = Lock()
        self.assertIsNone(lock.file)

    def test_default_lock_ttl(self):
        """Lock() default lock_ttl is: 30 """
        lock = Lock()
        self.assertEqual(lock._lock_ttl, 30)

    @mock.patch('uuid.uuid4')
    def test_lock_id(self,mock_uuid4):
        """Lock() check id"""
        import uuid
        from socket import gethostname

        host = gethostname()
        uid = uuid.uuid4()
        mock_uuid4.return_value = uid
        lock = Lock()
        self.assertEqual(lock.id,"%s:%s" % (host,uid))

    def test_default_callback(self):
        """Lock() default callback is: None """
        lock = Lock()
        self.assertIsNone(lock._callback)

    def test_default_callback_args(self):
        """Lock() default callback_args is: None """
        lock = Lock()
        self.assertTupleEqual(lock._callback_args, ())

    def test_key_locked(self):
        """Lock() check that _key_locked event clear """
        lock = Lock()
        self.assertFalse(lock._key_locked.is_set())

    def test_call_acquire(self):
        """Lock() test acquire method calls"""
        lock = Lock()
        lock._acquire_lock = mock.MagicMock()
        lock.acquire()
        lock._acquire_lock.assert_called_with()

    def test_is_acquired(self):
        """ Lock() True если блокировка захвачена"""
        lock = Lock()

        lock._key_locked.set()
        self.assertTrue(lock.is_acquired())

    def test_is_no_acquired(self):
        """ Lock() Flase если блокировка не захвачена"""
        lock = Lock()

        self.assertFalse(lock.is_acquired())

    def test_is_no_acquired(self):
        """ Lock() Flase если блокировка не захвачена"""
        lock = Lock()

        self.assertFalse(lock.is_acquired())

    def test_is_acquired_wait(self):
        """ Lock() ждать, пока блокировка будет захвачена и вернуть True"""
        lock = Lock()

        timer = RepeatableTimer(2, lock._key_locked.set)
        timer.start()
        self.assertTrue(lock.is_acquired(wait=True))


# TODO: Сделать тест с контролем вызова процедур
"""
    @mock.patch('procwrapper.lock.Lock._release_lock')
    @mock.patch('procwrapper.lock.Lock._delete_lock_key')
    def test_call_release(self, mock_release_lock, mock_delete_lock_key):
        #Lock() test release method calls#
        manager = mock.Mock()

        lock = Lock()
        #lock._release_lock = mock.MagicMock()
        #lock._delete_lock_key = mock.MagicMock()

        manager.m1, manager.m2 = mock_release_lock, mock_delete_lock_key
        lock.release()
        manager.assert_has_calls([mock.call.m1(), mock.call.m2()])
"""
