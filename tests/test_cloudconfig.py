# -*- coding: utf-8 -*-
from procwrapper.cloudconfig import CloudConfig
import unittest

try:
    import mock
except ImportError:
    from unittest import mock


class FakeGetter(object):
    def __init__(self, config):
        self._config = config

    def read(self):
        return self._config


class TestCloudConfig(unittest.TestCase):
    _FAKE_URL = 'http://soume.url/test/file.yaml'
    _FAKE_CLOUD_CONFIG = b'#!/bin/bash\n\necho "private_ipv4=$private_ipv4" >/etc/environment\necho "public_ipv4=$public_ipv4" >> /etc/environment\n\ncoreos-cloudinit -from-url https://s3.amazonaws.com/www.yaml'

    @mock.patch('procwrapper.cloudconfig.CloudConfig._switch_getter')
    def test_instantiate(self, switch_getter_mock):
        """ CloudConfig() can be instantiated """
        switch_getter_mock.return_value = FakeGetter(self._FAKE_CLOUD_CONFIG)
        conf = CloudConfig(self._FAKE_URL)
        self.assertIsNotNone(conf)

    @mock.patch('procwrapper.cloudconfig.CloudConfig._switch_getter')
    def test_default_config(self, switch_getter_mock):
        """CloudConfig() значение по умолчанию ._config должно быть: None"""
        switch_getter_mock.return_value = FakeGetter(self._FAKE_CLOUD_CONFIG)
        conf = CloudConfig()
        self.assertIsNone(conf._config)

    @mock.patch('procwrapper.cloudconfig.CloudConfig._switch_getter')
    def test_default_virtfs_loc(self, switch_getter_mock):
        """CloudConfig() значение по умолчанию .virtfs_loc должно быть: ''"""
        switch_getter_mock.return_value = FakeGetter(self._FAKE_CLOUD_CONFIG)
        conf = CloudConfig()
        self.assertEqual(conf._virtfs_loc, '')

    @mock.patch('procwrapper.cloudconfig.CloudConfig._switch_getter')
    def test_set_virtfs_loc(self, switch_getter_mock):
        """CloudConfig() установка значения .virtfs_loc в '/some/dir'"""
        test_val = '/some/dir'
        switch_getter_mock.return_value = FakeGetter(self._FAKE_CLOUD_CONFIG)
        conf = CloudConfig(virtfs_loc=test_val)
        self.assertEqual(conf._virtfs_loc, test_val)

    @mock.patch('procwrapper.cloudconfig.CloudConfig._switch_getter')
    def test_url(self, switch_getter_mock):
        """CloudConfig() значение _url должно быть: 'http://soume.url/test/file.yaml'"""
        switch_getter_mock.return_value = FakeGetter(self._FAKE_CLOUD_CONFIG)
        conf = CloudConfig(self._FAKE_URL)
        self.assertEqual(conf._url, self._FAKE_URL)

    @mock.patch('procwrapper.cloudconfig.CloudConfig._switch_getter')
    @mock.patch('procwrapper.cloudconfig.CloudConfig._write_cloud_config')
    def test_get_qemu_config(self, switch_getter_mock, write_cloud_config_mock, ):
        write_cloud_config_mock.return_value = True
        switch_getter_mock.return_value = FakeGetter(self._FAKE_CLOUD_CONFIG)
        qemu_config = [
            '-fsdev', 'local,id=conf,security_model=none,readonly,path=/virtfs',
            '-device', 'virtio-9p-pci,fsdev=conf,mount_tag=config-2'
        ]
        conf = CloudConfig(self._FAKE_URL)
        self.assertListEqual(conf.get_qemu_config(), qemu_config)

    @mock.patch('procwrapper.cloudconfig.CloudConfig._switch_getter')
    def test_get_cloud_config(self, switch_getter_mock):
        switch_getter_mock.return_value = FakeGetter(self._FAKE_CLOUD_CONFIG)
        conf = CloudConfig(self._FAKE_URL)

        self.assertEqual(conf._get_cloud_config(),self._FAKE_CLOUD_CONFIG.decode())
