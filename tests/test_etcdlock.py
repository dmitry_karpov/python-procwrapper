import unittest
import procwrapper
try:
    import mock
except ImportError:
    from unittest import mock


class TestEtcdLock(unittest.TestCase):
    def test_instantiate(self):
        """ client can be instantiated """
        lock = procwrapper.EtcdLock()
        self.assertIsNotNone(lock)

    def test_default_lock_type(self):
        """ default lock_type is: etcd """
        lock = procwrapper.EtcdLock()
        self.assertEqual(lock.lock_type, 'etcd')

    def test_default_lock_file(self):
        """ default lock_file is: None """
        lock = procwrapper.EtcdLock()
        self.assertIsNone(lock.file)

    def test_default_lock_ttl(self):
        """ default lock_ttl is: 30 """
        lock = procwrapper.EtcdLock()
        self.assertEqual(lock._lock_ttl, 30)

    def test_default_callback(self):
        """ default callback is: None """
        lock = procwrapper.EtcdLock()
        self.assertIsNone(lock._callback)

    def test_default_callback_args(self):
        """ default callback_args is: None """
        lock = procwrapper.EtcdLock()
        self.assertTupleEqual(lock._callback_args, ())

    def test_default_endpoints(self):
        """ default endpoints is: [{'protocol': 'http','host': '127.0.0.1','port': 4001},{'protocol': 'http','host': '127.0.0.1','port': 2379},] """
        lock = procwrapper.EtcdLock()
        self.assertListEqual(
            lock._etcd_endpoints,
            [
                {'protocol': 'http',
                 'host': '127.0.0.1',
                 'port': 4001},
                {'protocol': 'http',
                 'host': '127.0.0.1',
                 'port': 2379},
            ]
        )

    def test_set_lock_file(self):
        """ set lock_file is: /_locks/test_lock """
        lock = procwrapper.EtcdLock(lock_file='/_locks/test_lock')
        self.assertEqual(lock.file, '/_locks/test_lock')

    def test_set_lock_ttl(self):
        """ set lock_ttl is: 60 """
        lock = procwrapper.EtcdLock(lock_ttl=60)
        self.assertEqual(lock._lock_ttl, 60)

    def test_set_callback(self):
        """ set callback is: test_func """
        def test_func():
            pass
        lock = procwrapper.EtcdLock(callback=test_func)
        self.assertEqual(lock._callback, test_func)

    def test_set_callback_args(self):
        """ set callback_args is: arg1, arg2 """
        arg1 = 'test1'
        arg2 = 'wewe'
        lock = procwrapper.EtcdLock(callback_args=(arg1,arg2))
        self.assertTupleEqual(lock._callback_args, (arg1,arg2))

    def test_set_endpoints(self):
        """ set endpoints is: [{'protocol': 'http','host': '172.16.20.20','port': 2379},{'protocol': 'http','host': '172.16.20.40','port': 2379},] """
        lock = procwrapper.EtcdLock(
            endpoints=[
                {'protocol': 'http',
                 'host': '172.16.20.20',
                 'port': 2379},
                {'protocol': 'http',
                 'host': '172.16.20.40',
                 'port': 2379},
                ])
        self.assertListEqual(
            lock._etcd_endpoints,
            [
                {'protocol': 'http',
                 'host': '172.16.20.20',
                 'port': 2379},
                {'protocol': 'http',
                 'host': '172.16.20.40',
                 'port': 2379},
            ]
        )

    def test_timer_initialization(self):
        """ check that timer initialize correctly"""
        arg1 = 'CuUXcxCY4RyAg'
        arg2 = 'TfKs/.yU9Of4M'

        def test_func(*args):
            self.assertTupleEqual(args,(arg1, arg2))

        lock = procwrapper.EtcdLock(lock_ttl=1, callback=test_func, callback_args=(arg1, arg2))
        lock._release_lock = mock.MagicMock(return_value=True)
        lock._lock_timer.start()
        lock._lock_timer._timer.join()
