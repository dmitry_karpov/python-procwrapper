# -*- coding: utf-8 -*-
from procwrapper import Process
from procwrapper.lock import Lock
import unittest
try:
    import mock
except ImportError:
    from unittest import mock


class TestProcess(unittest.TestCase):
    _FAKE_PROG = '/usr/bin/true'
    _FAKE_ARG = '-h'

    def test_process_instantiate(self):
        """Process() can be instantiated """
        process = Process(
            prog=self._FAKE_PROG
        )
        self.assertIsNotNone(process)

    def test_set_bin(self):
        process = Process(
            prog=self._FAKE_PROG
        )
        self.assertEqual(process._bin, self._FAKE_PROG)

    def test_set_bin_args(self):
        process = Process(
            prog=self._FAKE_PROG,
            prog_args=self._FAKE_ARG,
        )
        self.assertEqual(process._bin_args, self._FAKE_ARG)

    def test_default_lock(self):
        """Default _lock is None"""
        process = Process(
            prog=self._FAKE_PROG
        )
        self.assertEqual(process._lock, None)

    def test_set_lock(self):
        test_val = Lock()
        process = Process(
            prog=self._FAKE_PROG,
            lock=test_val
        )
        self.assertIsInstance(process._lock, Lock)

    def test_default_proc(self):
        """Default _proc is None"""
        process = Process(
            prog=self._FAKE_PROG
        )
        self.assertEqual(process._proc, None)

    def test_default_proc_timeout(self):
        """Default _proc_timeout is 10 seconds"""
        test_val = 10
        process = Process(
            prog=self._FAKE_PROG
        )
        self.assertEqual(process._proc_timeout, test_val)

    def test_set_proc_timeout(self):
        """Test shutdown timer set"""
        test_val = 328
        process = Process(
            prog=self._FAKE_PROG,
            shutdown_timeout=test_val
        )
        self.assertEqual(process._proc_timeout, test_val)

    @mock.patch("procwrapper.lock.Lock.register_callback")
    def test_register_callback_function_call(self, lock_register_callback_mock):
        test_val = Lock()
        process = Process(
            prog=self._FAKE_PROG,
            lock=test_val
        )
        self.assertTrue(lock_register_callback_mock.called)

    def test_kill_proc_call_without_proc(self):
        lock = Lock()
        process = Process(
            prog=self._FAKE_PROG,
            lock=lock,
        )
