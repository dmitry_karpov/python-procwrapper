# -*- coding: utf-8 -*-
from procwrapper import Qemu
import unittest
try:
    import mock
except ImportError:
    from unittest import mock


class TestQemu(unittest.TestCase):
    @mock.patch('procwrapper.Qemu._prepare_config')
    def test_instantiate(self, mock_prepare_config):
        """Qemu() can be instantiated """
        mock_prepare_config.return_value = True
        qemu = Qemu()
        self.assertIsNotNone(qemu)

    @mock.patch('procwrapper.Qemu._prepare_config')
    def test_default_bin(self, mock_prepare_config):
        """Qemu() default _bin is: /usr/bin/kvm"""
        mock_prepare_config.return_value = True
        qemu = Qemu()
        self.assertEqual(qemu._bin, '/usr/bin/kvm')

    @mock.patch('procwrapper.Qemu._prepare_config')
    def test_default_qmp_sock(self, mock_prepare_config):
        mock_prepare_config.return_value = True
        qemu = Qemu()
        self.assertEqual(qemu._qmp_sock, '/var/run/qemu/sock/qmp.sock')

    @mock.patch('procwrapper.Qemu._prepare_config')
    def test_default_ga_sock(self, mock_prepare_config):
        mock_prepare_config.return_value = True
        qemu = Qemu()
        self.assertEqual(qemu._ga_sock, '/var/run/qemu/sock/qemu-ga.sock')

    @mock.patch('procwrapper.Qemu._prepare_config')
    def test_default_bin_args(self, mock_prepare_config):
        """Qemu() default _bin_args is: []"""
        mock_prepare_config.return_value = True
        qemu = Qemu()
        args = ['-qmp', 'unix:%s,server,nowait' % qemu._qmp_sock] + \
               [
                   '-chardev', 'socket,path=%s,server,nowait,id=qga0' % qemu._ga_sock,
                   '-device', 'virtio-serial',
                   '-device', 'virtserialport,chardev=qga0,name=org.qemu.guest_agent.0'
               ]
        self.assertListEqual(qemu._bin_args, args)

    @mock.patch('procwrapper.Qemu._prepare_config')
    def test_default_lock(self, mock_prepare_config):
        """Qemu() default lock is: None"""
        mock_prepare_config.return_value = True
        qemu = Qemu()
        self.assertIsNone(qemu._lock)

    @mock.patch('procwrapper.Qemu._prepare_config')
    def test_default_shutdown_timeout(self, mock_prepare_config):
        """Qemu() default shutdown_timeout is: 30"""
        mock_prepare_config.return_value = True
        qemu = Qemu()
        self.assertEqual(qemu._proc_timeout,30)

    @mock.patch('procwrapper.Qemu._prepare_config')
    def test_default_bridge_if(self, mock_prepare_config):
        mock_prepare_config.return_value = True
        qemu = Qemu()
        self.assertEqual(qemu._bridge_if, 'qemu0')
