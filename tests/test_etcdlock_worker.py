from threading import Thread, Event, Lock
import unittest
import procwrapper.timer
import procwrapper.etcdlock


class TestEtcdLockWorker(unittest.TestCase):
    def test_instantiate(self):
        """ worker can be instantiated """
        def test_func(*args):
            pass

        t = procwrapper.timer.RepeatableTimer(10, test_func, args=('qqq', 'www'))
        endpoint = {'protocol': 'http',
             'host': '172.16.20.20',
             'port': 2379
        }

        worker = procwrapper.etcdlock.Worker(Lock(), 30, t, endpoint, '/_locks/test_lock', 'null', Event())
        self.assertIsNotNone(worker)
