Procwrapper
=======================

A set of utilities that allows you to run a process in a distributed system. Easy to extend by adding new ways of locks and wrappers around different processes.