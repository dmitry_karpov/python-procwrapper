"""A setuptools based setup module.

See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='procwrapper',
    version='0.0.2+git',
    description='A sample Python project',
    long_description=long_description,
    url='https://github.com/pypa/sampleproject',
    author='Dmitry K.',
    author_email='s--m--i--l--e@yandex.ru',
    license='MIT',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    keywords='qemu wrapper',
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    install_requires=['python-etcd'],
    dependency_links=[
        'git+https://bitbucket.org/dmitry_karpov/python-qmp.git#egg=qmp-2.5'
    ],
    test_suite="tests",
    extras_require={
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },
)