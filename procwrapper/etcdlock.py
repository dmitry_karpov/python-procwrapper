# -*- coding: utf-8 -*-

import etcd
from threading import Thread, Event, Lock
import time
from procwrapper import lock, timer
import logging

_etcd_endpoints_struct = [
    {'protocol': 'http',
     'host': '127.0.0.1',
     'port': 4001},
    {'protocol': 'http',
     'host': '127.0.0.1',
     'port': 2379},
]


class EtcdLock(lock.Lock):
    def __init__(self, lock_type='etcd', lock_file=None, lock_ttl=30, endpoints=_etcd_endpoints_struct,
                 callback=None, callback_args=()):
        super(EtcdLock, self).__init__(lock_type, lock_file, lock_ttl, callback, callback_args=callback_args)
        self._etcd_endpoints = endpoints
        self._lock_timer = timer.RepeatableTimer(self._lock_ttl/2, self._die_lock)
        self._workers_lock = Lock()
        self._workers = [
            Worker(
                self._workers_lock,
                self._lock_ttl,
                self._lock_timer,
                endpoint,
                self.file,
                self.id,
                self._key_locked
            ) for endpoint in self._etcd_endpoints
        ]

    def _acquire_lock(self):
        [w.start() for w in self._workers]

    def _die_lock(self):
        self._callback(*self._callback_args)
        self._release_lock()

    def _release_lock(self):
        [w.stop() for w in self._workers]
        [w.join() for w in self._workers]
        self._lock_timer.cancel()

    def wait(self):
        [w.join() for w in self._workers]


class Worker(object):
    def __init__(self, active_flag, ttl, timer, endpoint, lock, lock_id, key_lock):
        self._event = Event()
        self._active_flag = active_flag
        self._ttl = ttl
        self._timer = timer
        self._endpoint = endpoint
        self._lock = lock
        self._lock_id = lock_id
        self._key_lock = key_lock
        self._thread = Thread(
            target=self._worker,
        )
        self._client = etcd.Client(read_timeout=self._ttl/3, **self._endpoint)

    def _worker(self):
        self._event.wait()
        logging.info("%s: %s: Starting." % (self._thread.ident,self._thread.name))
        while self._event.is_set():
            with self._active_flag:
                try:
                    logging.info ("%s: %s: The lock is aquired." % (self._thread.ident,self._thread.name))
                    self._client.get("/")
                    logging.info (
                        "{ident}: {name}: Access to ETCD obtained through: {protocol}://{host}:{port}".format(
                            ident=self._thread.ident,
                            name=self._thread.name,
                            **self._endpoint)
                    )
                    while self._event.is_set():
                        try:
                            if self._client.read(self._lock).value == self._lock_id:
                                self._client.write(self._lock, self._lock_id, ttl=self._ttl)
                                if not self._key_lock.is_set():
                                    self._key_lock.set()
                        except etcd.EtcdKeyNotFound:
                            self._client.write(self._lock, self._lock_id, ttl=self._ttl)
                            if not self._key_lock.is_set():
                                self._key_lock.set()
                        finally:
                            if self._timer.running():
                                self._timer.cancel()
                                logging.warning("%s: %s: The timer is stopped." % (self._thread.ident,self._thread.name))
                            time.sleep(self._ttl/2)
                except etcd.EtcdException as e:
                    logging.error("{ident}: {name}: Cannot establish connection via: {protocol}://{host}:{port}".format(
                            ident=self._thread.ident,
                            name=self._thread.name,
                            **self._endpoint)
                    )
                    if not self._timer.running():
                        self._timer.start()
                        logging.warning("%s: %s: Loss of connectivity. Timer started." % (self._thread.ident, self._thread.name))
            time.sleep(1)
        else:
            logging.info("%s: %s: Shutdown." % (self._thread.ident, self._thread.name))

    def start(self):
        self._event.set()
        self._thread.start()

    def stop(self):
        self._event.clear()

    def join(self):
        self._thread.join()
