# -*- coding: utf-8 -*-

from socket import gethostname
import uuid
from threading import Event


class Lock(object):
    def __init__(self, lock_type=None, lock_file=None, lock_ttl=30, callback=None, callback_args=()):
        self.lock_type = lock_type
        self.file = lock_file
        self._lock_ttl = lock_ttl
        self.id = "%s:%s" % (gethostname(), uuid.uuid4())
        self._callback = callback
        self._callback_args = callback_args
        self._key_locked = Event()
        self._key_locked.clear()

    def _acquire_lock(self):
        pass

    def _release_lock(self):
        pass

    def _delete_lock_key(self):
        pass

    def acquire(self):
        self._acquire_lock()

    def release(self):
        if self._key_locked.is_set():
            self._key_locked.clear()
        self._release_lock()
        self._delete_lock_key()

    def is_acquired(self, wait=False):
        return True if (not wait and self._key_locked.is_set()) or (wait and self._key_locked.wait()) else False

    def wait(self):
        pass

    def register_callback(self, function=None, args=()):
        self._callback = function
        self._callback_args = args
