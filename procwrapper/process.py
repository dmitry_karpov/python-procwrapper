# -*- coding: utf-8 -*-

import os
from subprocess import Popen, PIPE, STDOUT
import time
import logging


class Process(object):

    def __init__(self, prog, prog_args=[], lock=None, shutdown_timeout=10):
        self._bin = prog
        self._bin_args = prog_args
        self._lock = lock
        self._proc = None
        self._proc_timeout = shutdown_timeout
        if self._lock:
            self._lock.register_callback(self._kill_proc)

    def _kill_proc(self):
        if self._proc is not None:
            logging.info("%s: Send KILL signal to process %s" % (self._proc.pid, self._bin))
            self._proc.kill()

    def start(self):
        if self._lock:
            self._lock.acquire()
            if self._lock.is_acquired(wait=True):
                logging.info("%s: Lock acquired %s : %s" %(self._lock.lock_type, self._lock.file, self._lock.id))
        self._proc = Popen(
            [ self._bin ] + self._bin_args,
            stdout=PIPE,
            stderr=STDOUT,
            stdin=PIPE,
            preexec_fn=os.setpgrp
        )
        return self

    def stop(self):
        logging.info("%s: Try normal shutdown" % self._proc.pid)
        self._proc.terminate()
        timeout = time.time() + self._proc_timeout
        while time.time() < timeout:
            pid, status = os.waitpid(self._proc.pid, os.WNOHANG)
            if pid != 0:
                logging.info("----- child %d terminated with status: %d" %(pid, status))
                break
            time.sleep(1)
        else:
            self._kill_proc()
        if self._lock:
            self._lock.release()

    def sigterm_handler(self, _signo, _stack_frame):
        logging.info('SIGTERM received')
        self.stop()

    def communicate(self):
        line = '###'
        while line:
            line = self._proc.stdout.readline().decode()
            print (line.strip())
