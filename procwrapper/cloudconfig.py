# -*- coding: utf-8 -*-

from os import path, makedirs, environ
from urllib.parse import urlparse
from urllib.request import urlopen
from urllib.error import HTTPError
import json
import logging
import uuid


class CloudConfig(object):
    def __init__(self, url=None, virtfs_loc=''):
        self._config = None
        self._url = url
        self._virtfs_loc = virtfs_loc
        self._getter = self._switch_getter()

    def _switch_getter(self):
        return {
            'http': urlopen(self._url),
            'https': urlopen(self._url),
            'file': urlopen(self._url),
        }.get(
            urlparse(self._url).scheme
        )

    def _get_cloud_config(self, counter=1):
        max_tries = 3
        try:
            logging.info('Attempt %s to get the cloud-config file from URL: %s' %(counter, self._url))
            return self._getter.read().decode() if self._getter else None
        except HTTPError as e:
            logging.error('During query execution we got an exception: Code: %s Reason: %s' % (e.code, e.reason))
            if counter < max_tries and (e.code < 400 or e.code > 500):
                return self._get_cloud_config(counter+1)
            else:
                raise

    def _write_cloud_config(self):
        config_path='%s/virtfs/openstack/latest/user_data' % self._virtfs_loc
        metadata_path='%s/virtfs/openstack/latest/meta_data.json' % self._virtfs_loc
        if not path.exists(path.dirname(config_path)):
            makedirs(path.dirname(config_path))
        with open(config_path, 'w') as f:
            f.write(self._get_cloud_config())
        with open(metadata_path, 'w') as f:
            json.dump({'uuid': environ.get('UUID', str(uuid.uuid4()))}, f)
        return True

    def get_qemu_config(self):
        qemu_config= [
            '-fsdev', 'local,id=conf,security_model=none,readonly,path=%s/virtfs' % self._virtfs_loc,
            '-device', 'virtio-9p-pci,fsdev=conf,mount_tag=config-2'
        ]
        return self._write_cloud_config() and qemu_config
