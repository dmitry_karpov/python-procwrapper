# -*- coding: utf-8 -*-

from procwrapper import process, timer, cloudconfig
import qmp
import os.path
import sys
import time
import traceback
import logging


class Qemu(process.Process):
    _CPU_FLAG = 'qemu64,+ssse3,+sse4.1,+sse4.2,+x2apic'

    def __init__(self, prog='/usr/bin/kvm', prog_args=[], lock=None, cloud_config_url=None, bridge_if='qemu0',
                 shutdown_timeout=30):
        super(Qemu, self).__init__(prog, prog_args, lock, shutdown_timeout)
        self._qmp_sock = '/var/run/qemu/sock/qmp.sock'
        self._ga_sock = '/var/run/qemu/sock/qemu-ga.sock'
        self._qemu_mon = qmp.QEMUMonitorProtocol(self._qmp_sock)
        self._bin_args = self._bin_args + \
                         ['-qmp', 'unix:%s,server,nowait' % self._qmp_sock] + \
                         ['-chardev', 'socket,path=%s,server,nowait,id=qga0' % self._ga_sock,
                          '-device', 'virtio-serial',
                          '-device', 'virtserialport,chardev=qga0,name=org.qemu.guest_agent.0']
        if cloud_config_url:
            self._bin_args = self._bin_args + cloudconfig.CloudConfig(cloud_config_url).get_qemu_config()
        self._bridge_if = bridge_if
        self._prepare_config()

    def _prepare_config(self):
        if not os.path.exists('/etc/qemu'):
            os.makedirs('/etc/qemu')
            with open('/etc/qemu/bridge.conf', 'w') as f:
                f.write("allow %s \n" % self._bridge_if)
        if not os.path.exists('/var/run/qemu/sock'):
            os.makedirs('/var/run/qemu/sock')

    def stop(self):
        logging.info('Starting %s sec. kill timer' % self._proc_timeout)
        kill_timer = timer.RepeatableTimer(self._proc_timeout, self._kill_proc)
        kill_timer.start()
        try:
            self._qemu_mon.connect()
            logging.info('Connected to qmp socket.')
            logging.info('Sending system_powerdown command to guest', end="")
            self._qemu_mon.command('system_powerdown')
            status = self._qemu_mon.command('query-status')
            while 'running' in status and status['running']:
                print('.', end="", flush=True)
                status = self._qemu_mon.command('query-status')
                time.sleep(1)
        except TypeError:
            pass
        except AttributeError:
            pass
        except ConnectionResetError:
            logging.error('\nConnectionResetError exception')
        except ConnectionRefusedError:
            # TODO: terminate
            logging.error('\nConnectionRefusedError exception')
        except Exception as e:
            logging.error("Exception in user code:")
            print("-"*60)
            traceback.print_exc(file=sys.stdout)
            print("-"*60)
        finally:
            logging.info("\nDone.")

        self._proc.wait()
        kill_timer.cancel()

        if self._lock:
            self._lock.release()
